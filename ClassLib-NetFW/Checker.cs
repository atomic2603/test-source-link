﻿namespace ClassLib_NetFw
{
    public static class Checker
    {
        public static bool IsNumber(string input)
        {
            var result = int.TryParse(input, out var number);
            return result;
        }
    }
}
